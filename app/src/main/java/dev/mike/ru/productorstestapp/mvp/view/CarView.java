package dev.mike.ru.productorstestapp.mvp.view;

import java.util.List;

import dev.mike.ru.productorstestapp.model.Cars;

public interface CarView extends View {
    void showMessage(String message);

    void updateUI(List<Cars> cars);

    void showProgressBar();

    void hideProgressBar();
}
