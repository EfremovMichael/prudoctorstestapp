package dev.mike.ru.productorstestapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dev.mike.ru.productorstestapp.adapter.CarAdapter;
import dev.mike.ru.productorstestapp.model.Cars;
import dev.mike.ru.productorstestapp.mvp.presenter.CarPresenter;
import dev.mike.ru.productorstestapp.mvp.view.CarView;
import dev.mike.ru.productorstestapp.network.Network;

public class CarFragment extends Fragment implements CarView {

    private CarAdapter mCarAdapter;
    private RecyclerView mRecyclerView;
    private CarPresenter mCarPresenter;
    private ProgressBar mProgressBar;

    public CarFragment() {
    }

    @Override
    public void onDestroy() {
        mCarPresenter.detachView();
        super.onDestroy();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_car, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        mProgressBar = view.findViewById(R.id.progressBar);
        mCarPresenter = new CarPresenter(new Network());
        mCarPresenter.attachView(this);

        mRecyclerView = view.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mCarPresenter.loadCars();
        mCarAdapter = new CarAdapter();
    }

    @Override
    public void showMessage(String message) {
        if (mProgressBar.getVisibility() == View.VISIBLE){
            hideProgressBar();
        }
        Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
        getActivity().onBackPressed();
    }

    @Override
    public void updateUI(List<Cars> cars) {
        mCarAdapter.setCars(cars);
        mRecyclerView.setAdapter(mCarAdapter);
    }

    @Override
    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }
}
