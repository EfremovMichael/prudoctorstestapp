package dev.mike.ru.productorstestapp.network;

import java.util.List;

import dev.mike.ru.productorstestapp.model.Car;
import dev.mike.ru.productorstestapp.model.CarModel;
import io.reactivex.Observable;
import retrofit2.http.GET;

public interface CarApi {

    @GET("api/v1/cars")
    Observable<List<Car>> getCar();

    @GET("/api/v1/car_model_ref")
    Observable<List<CarModel>> getCarModel();
}
