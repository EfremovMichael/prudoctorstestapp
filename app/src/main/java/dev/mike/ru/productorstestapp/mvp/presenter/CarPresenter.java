package dev.mike.ru.productorstestapp.mvp.presenter;

import dev.mike.ru.productorstestapp.mvp.view.CarView;
import dev.mike.ru.productorstestapp.network.Network;
import io.reactivex.disposables.Disposable;

public class CarPresenter implements Presenter<CarView> {

    private CarView mView;
    private Network mNetwork;
    private Disposable mDisposable;

    public CarPresenter(Network network) {
        mNetwork = network;
    }

    @Override
    public void attachView(CarView view) {
        this.mView = view;
    }

    @Override
    public void detachView() {
        mDisposable.dispose();
        mView = null;
    }

    public void loadCars(){
        mView.showProgressBar();
        mDisposable = mNetwork.getCars().subscribe(cars -> {
            mView.hideProgressBar();
            mView.updateUI(cars);
        }, throwable -> mView.showMessage(throwable.getMessage()));
    }

}
