package dev.mike.ru.productorstestapp.mvp.presenter;

import dev.mike.ru.productorstestapp.mvp.view.View;

public interface Presenter<T extends View> {

    void attachView(T view);
    void detachView();
}
