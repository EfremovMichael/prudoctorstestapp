package dev.mike.ru.productorstestapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import dev.mike.ru.productorstestapp.R;
import dev.mike.ru.productorstestapp.model.Cars;

public class CarAdapter extends RecyclerView.Adapter<CarAdapter.ViewHolder> {

    private List<Cars> cars;


    public void setCars(List<Cars> cars) {
        this.cars = cars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.car_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvCarId.setText(String.valueOf(cars.get(position).getId()));
        holder.tvCarModel.setText(cars.get(position).getModel());
        holder.tvCarYear.setText(String.valueOf(cars.get(position).getYear()));
        holder.tvCarOwner.setText(cars.get(position).getOwner());
    }

    @Override
    public int getItemCount() {
        return cars.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCarId;
        private TextView tvCarModel;
        private TextView tvCarYear;
        private TextView tvCarOwner;

        ViewHolder(@NonNull View view) {
            super(view);
            tvCarId = view.findViewById(R.id.tvCarId);
            tvCarModel = view.findViewById(R.id.tvCarModel);
            tvCarYear = view.findViewById(R.id.tvCarYear);
            tvCarOwner = view.findViewById(R.id.tvCarOwner);
        }
    }
}
