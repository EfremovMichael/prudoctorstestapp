package dev.mike.ru.productorstestapp.network;

import java.util.ArrayList;
import java.util.List;

import dev.mike.ru.productorstestapp.model.Car;
import dev.mike.ru.productorstestapp.model.CarModel;
import dev.mike.ru.productorstestapp.model.Cars;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Network {

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://5b90e1cc3ef10a001445d113.mockapi.io/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

    private Observable<List<Car>> getCar = retrofit.create(CarApi.class)
            .getCar()
            .subscribeOn(Schedulers.io());

    private Observable<List<CarModel>> getCarModel = retrofit.create(CarApi.class)
            .getCarModel()
            .subscribeOn(Schedulers.io());


    public Observable<List<Cars>> getCars() {
        return Observable.zip(getCar, getCarModel, this::merge)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    private List<Cars> merge(List<Car> cars, List<CarModel> carModels) {
        List<Cars> carList = new ArrayList<>();
        for (Car car : cars) {
            for (CarModel carModel : carModels) {
                if (car.getModelId() == carModel.getId()) {
                    carList.add(new Cars(car.getId(), carModel.getTitle(), car.getYear(), car.getOwner()));
                }
            }
        }
        return carList;
    }


}
