package dev.mike.ru.productorstestapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cars {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("model_id")
    @Expose
    private String model;
    @SerializedName("year")
    @Expose
    private int year;
    @SerializedName("owner")
    @Expose
    private String owner;

    public Cars(int id, String model, int year, String owner) {
        this.id = id;
        this.model = model;
        this.year = year;
        this.owner = owner;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
