package dev.mike.ru.productorstestapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import dev.mike.ru.productorstestapp.network.NetworkUtils;

public class MainFragment extends Fragment implements View.OnClickListener {

    public MainFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Button mMainButton = view.findViewById(R.id.btnMain);
        mMainButton.setOnClickListener(this);

        if (!NetworkUtils.isOnline(getContext())) {
            mMainButton.setClickable(false);
            Toast.makeText(getContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
        }

        return view;
    }

    @Override
    public void onClick(View v) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, new CarFragment())
                .addToBackStack("back")
                .commit();
    }
}
